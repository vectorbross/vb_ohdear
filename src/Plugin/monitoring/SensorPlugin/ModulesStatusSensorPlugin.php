<?php
/**
 * @file
 * Contains \Drupal\vb_ohdear\Plugin\monitoring\SensorPlugin\ModulesStatusSensorPlugin.
 */

namespace Drupal\vb_ohdear\Plugin\monitoring\SensorPlugin;

use Drupal\monitoring\Result\SensorResultInterface;
use Drupal\monitoring\SensorPlugin\SensorPluginBase;
use Drupal\update\UpdateFetcherInterface;
use Drupal\update\UpdateManagerInterface;

/**
 * Monitors status of modules.
 *
 * @SensorPlugin(
 *   id = "monitoring_modules_status",
 *   label = @Translation("Modules status"),
 *   description = @Translation("Monitors status of modules."),
 *   addable = FALSE
 * )
 *
 */
class ModulesStatusSensorPlugin extends SensorPluginBase {

  /**
   * {@inheritdoc}
   */
  protected $configurableValueType = FALSE;

  /**
   * {@inheritdoc}
   */
  public function runSensor(SensorResultInterface $result) {
    $result->setStatus(SensorResultInterface::STATUS_INFO);

    $available = update_get_available();
    $project_data = update_calculate_project_data($available);

    $this->checkContrib($result, $project_data);
  }

  /**
   * Checks contrib status and sets sensor status message.
   *
   * @param \Drupal\monitoring\Result\SensorResultInterface $result
   * @param array $project_data
   */
  protected function checkContrib(SensorResultInterface $result, $project_data) {

    $important_updates = [];

    foreach ($project_data as $info) {
      $status_text = $this->getStatusText($info['status']);

      if (!isset($important_updates[$status_text])) {
        $important_updates[$status_text] = [];
      }
      $important_updates[$status_text][] = $info;
    }

    $modules_array = [];

    foreach ($important_updates as $status_text => $update_info) {
      foreach ($update_info as $info) {
        if($info['info']['project'] == 'drupal') {
          if ($status_text == 'current') {
            $modules_array[] = [
              'module' => 'Core',
              'project' => 'drupal',
              'status' => $status_text,
              'current' => isset($info['existing_version']) ? $info['existing_version'] : NULL,
              'recommended' => NULL,
            ];
          } else {
            $modules_array[] = [
              'module' => 'Core',
              'project' => 'drupal',
              'status' => $status_text,
              'current' => isset($info['existing_version']) ? $info['existing_version'] : NULL,
              'recommended' => isset($info['latest_version']) ? $info['latest_version'] : NULL,
            ];
          }
          continue;
        }
        if ($status_text == 'NOT SECURE') {
          $modules_array[] = [
            'module' => $info['info']['name'],
            'project' => $info['info']['project'],
            'status' => $status_text,
            'current' => isset($info['existing_version']) ? $info['existing_version'] : NULL,
            'recommended' => isset($info['recommended']) ? $info['recommended'] : NULL,
          ];
        }
        else {
          $modules_array[] = [
            'module' => $info['info']['name'],
            'project' => $info['info']['project'],
            'status' => $status_text,
            'current' => isset($info['existing_version']) ? $info['existing_version'] : NULL,
            'recommended' => NULL,
          ];
        }
      }
    }

    $result->addStatusMessage(json_encode($modules_array));

  }

  /**
   * Gets status text.
   *
   * @param int $status
   *   One of UpdateManagerInterface::* constants.
   *
   * @return string
   *   Status text.
   */
  protected function getStatusText($status) {
    switch ($status) {
      case UpdateManagerInterface::NOT_SECURE:
        return 'NOT SECURE';
        break;

      case UpdateManagerInterface::CURRENT:
        return 'current';
        break;

      case UpdateManagerInterface::REVOKED:
        return 'version revoked';
        break;

      case UpdateManagerInterface::NOT_SUPPORTED:
        return 'not supported';
        break;

      case UpdateManagerInterface::NOT_CURRENT:
        return 'update available';
        break;

      case UpdateFetcherInterface::UNKNOWN:
      case UpdateFetcherInterface::NOT_CHECKED:
      case UpdateFetcherInterface::NOT_FETCHED:
      case UpdateFetcherInterface::FETCH_PENDING:
        return 'unknown';
        break;
    }
  }

}
